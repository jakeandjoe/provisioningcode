-- ADD EC USER
declare

    x number;
    trainer char(1) := 'N';
    asid number := 43284;
    userName varchar2(15) := 'Lariviere_D';
    password varchar2(15) := 'changeme';
    lastName varchar2(15) := 'Lariviere';
    firstName varchar2(15) := 'Deborah';
    emailaddress varchar2(100) := 'DLariviere@KW.com';

begin

    x := add_ec_er_user (asid,username,password,'EC',firstname,lastname,emailaddress,trainer,'N',null);
    dbms_output.put_line(x);

end;

--ER28873 BME-R
--eR28873 bm3-R