--there are 2 types of setups BME where there is either more than one office or one mls and BMO for just a single office
--in BME setups EC users don't need to have agent records in the mls, they can be anyone
--for BMO users they are supposed to have agent record but they can approve staff with records if they give us another valid id


select terradatum.customer_s.nextval from dual
--use the next customer sid for the authority sid - add 1 to your result
insert into metrics.authority (authority_sid, description, modified_date, modified_by, status_code)
values (43023, 'RE/MAX PowerPro Realty', sysdate, 'METRICS','A')

commit
--add additional mls and set the order by to the next number
insert into metrics.authority_mls_map (authority_sid, mls_sid, order_by, modified_date, modified_by)
values (43023,76, 1, sysdate, 'METRICS')


--always run this one
insert into metrics.authority_options
  (authority_sid, product_code, key, value, users_type_code)
select  43023, product_code, key, value, users_type_code from metrics.option_list

--adjust this for either BME or BMO setup and if all tools for BMO make sure to include the <> PM since it doesn't work for BMOs
insert into metrics.users_tab_map
  (authority_sid, product_code, tool_code, tab_code)
select 43023, product_code, tool_code, tab_code from metrics.product_tool_tab_map where tab_code <> 'CA' and product_code = 'BM'
and tool_code <> 'PM'

commit

